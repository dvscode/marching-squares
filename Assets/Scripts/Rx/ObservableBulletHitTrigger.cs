using UnityEngine;
using System.Collections;
using UniRx;
using UniRx.Triggers;

public class ObservableBulletHitTrigger : ObservableTriggerBase {
	Subject<Unit> onBulletHit;

	void OnCollisionEnter (Collision collisionInfo) {
		if (onBulletHit != null) {
			onBulletHit.OnNext (Unit.Default);
		}
	}

	public IObservable<Unit> OnBulletHitAsObservable () {
		return onBulletHit ?? (onBulletHit = new Subject<Unit>());
	}

	protected override void RaiseOnCompletedOnDestroy () {
		onBulletHit.OnCompleted ();
	}
}

