using UnityEngine;
using System.Collections;
using Zenject;

namespace MarchingSquares {
	public class VirtualInput : IInitializable
	{
		public enum Axis {
			Vertical = 0,
			Horizontal,
			LookVertical,
			LookHorizontal,
			Zoom
		}

		public enum Button {
			Fire
		}

		float [] axisData;
		bool [] buttonData;

		int axisCount;
		int buttonCount;
		
		public void Initialize () {
			axisCount = System.Enum.GetValues(typeof(Axis)).Length;
			axisData = new float[axisCount];
			for (int idx = 0; idx < axisCount; idx++) {
				axisData[idx] = 0;
			}

			buttonCount = System.Enum.GetValues(typeof(Button)).Length;
			buttonData = new bool[buttonCount];
			
			for (int idx = 0; idx < buttonCount; idx++) {
				buttonData[idx] = false;
			}
		}

		public float GetRotation (Axis horizontal, Axis vertical) {
			float h = axisData[(int) horizontal];
			float v = axisData[(int) vertical];
			return (h != 0 || v != 0) ? Mathf.Atan2 (h, v) : 0;
		}

		public float GetAxis(Axis axis) {
			return axisData[(int) axis];
		}

		public Vector2 GetAxis (Axis horizontal, Axis vertical) {
			return new Vector2 (axisData[(int) horizontal], axisData[(int) vertical]);
		}

		public void UpdateAxis (ref Vector2 axis, Axis horizontal, Axis vertical) {
			axis.Set (axisData[(int) horizontal], axisData[(int) vertical]);
		}

		public void SetAxis(Axis axis, float value) {
			axisData[(int) axis] = value;
		}

		public bool GetButton(Button button) {
			return buttonData[(int) button];
		}
		
		public void SetButton(Button button, bool value) {
			buttonData[(int) button] = value;
		}
	}
}

