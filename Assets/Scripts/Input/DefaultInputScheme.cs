﻿using UnityEngine;
using System.Collections;
using Zenject;

namespace MarchingSquares {
	public class DefaultInputScheme : IInitializable, ITickable {

		VirtualInput _input;

		public DefaultInputScheme (VirtualInput input) {
			_input = input;
		}

		public void Initialize () {
			_input.SetAxis (VirtualInput.Axis.Horizontal, 0);
			_input.SetAxis (VirtualInput.Axis.Vertical, 0);
			_input.SetAxis (VirtualInput.Axis.Zoom, 0);
			Input.simulateMouseWithTouches = false;
		}

		public void Tick () {
			_input.SetAxis (VirtualInput.Axis.Horizontal, Input.GetAxis("Horizontal"));
			_input.SetAxis (VirtualInput.Axis.Vertical, Input.GetAxis("Vertical"));
			_input.SetButton (VirtualInput.Button.Fire, Input.GetKeyDown (KeyCode.Space));

			if (Input.touchCount == 2) {
				// pinch
				var touch1 = Input.GetTouch (0);
				var touch2 = Input.GetTouch (1);
				var prevDst = ((touch1.position - touch1.deltaPosition) - (touch2.position - touch2.deltaPosition)).magnitude;
				var newDst = (touch1.position - touch2.position).magnitude;
				_input.SetAxis (VirtualInput.Axis.Zoom, (prevDst - newDst) / (Screen.height / 5));
			} else {
				_input.SetAxis (VirtualInput.Axis.Zoom, Input.mouseScrollDelta.y);
			}
		}
	}
}
