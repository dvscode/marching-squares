using UnityEngine;
using System.Collections;

namespace MarchingSquares {
	public class BulletView : MonoBehaviour {

		SphereCollider _collider;
		Transform _transform;
		Rigidbody _rigidbody;

		public void Initialize (float colliderSize) {

			_collider = gameObject.AddComponent <SphereCollider>();
			_collider.radius = 0.5f;
			_rigidbody = gameObject.AddComponent <Rigidbody>();
			_rigidbody.isKinematic = false;
			_rigidbody.useGravity = false;
			_rigidbody.freezeRotation = true;
			_rigidbody.collisionDetectionMode = CollisionDetectionMode.Continuous;
			_rigidbody.interpolation = RigidbodyInterpolation.Interpolate;
			_transform = gameObject.GetComponent <Transform>();
			_transform.localScale = Vector3.one * colliderSize;
		}

		public Vector3 Position {
			get {
				return _transform.position;
			}
		}

		public void Teleport (Vector3 position) {
			_transform.position = position;
		}

		public void Move (Vector3 targetPosition) {
			// raycast movement
			RaycastHit hitInfo;
			if (Physics.Linecast (Position, targetPosition, out hitInfo)) {
				targetPosition = hitInfo.point;
			}
			_rigidbody.MovePosition (targetPosition);
		}
	}
}
