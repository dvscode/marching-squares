﻿using UnityEngine;
using System.Collections;
using System;

public class PlayerView : MonoBehaviour {
	
	Rigidbody _rigidbody;
	Transform _transform;
	SphereCollider _collider;

	public Vector3 Position {
		get {
			return _transform.position;
		}
	}

	public void Initialize (float colliderSize) {
		_collider = gameObject.AddComponent <SphereCollider>();
		_collider.radius = 0.5f;
		_rigidbody = gameObject.AddComponent <Rigidbody>();
		_rigidbody.isKinematic = false;
		_rigidbody.useGravity = true;
		_rigidbody.freezeRotation = true;
		_transform = gameObject.GetComponent <Transform>();
		_transform.localScale = Vector3.one * colliderSize;
	}

	public void SetPosition (Vector3 position) {
		_transform.position = position;
	}

	public Vector3 MoveToIfPossible (Vector3 targetPosition) {
		var currentPosition = _transform.position;
		var direction = (targetPosition - currentPosition).normalized;

		// raycast possible movement
		RaycastHit hit;
		if (Physics.Raycast (_transform.position, direction, out hit, _collider.radius)) {
			targetPosition = hit.point - direction * (_collider.radius - .01f);
		}

		_rigidbody.MovePosition (targetPosition);
		return targetPosition;
	}

}
