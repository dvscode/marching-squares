﻿using UnityEngine;
using System.Collections;
using Zenject;
using Zenject.Commands;

namespace MarchingSquares {

	public class MainInstaller : MonoInstaller {

		public Settings sceneSettings;

		[InjectOptional]
		public int startingBullets = 100;

		public override void InstallBindings () {
		
			InstallGame ();
			InstallTriggers ();
			InstallSettings ();
		}

		public override void Start ()
		{
			base.Start ();
			// force UI Creation
			Container.InstantiatePrefab (sceneSettings.ui);
		}

		void InstallGame () {
			Container.Bind <ILogger> ().ToSingle <UnityLogger> ();

			Container.Bind <GameManager> ().ToSingle ();
			Container.Bind <IInitializable> ().ToSingle <GameManager> ();
			Container.Bind <IFixedTickable> ().ToSingle <GameManager> ();

			Container.Bind <PlayerView> ().ToTransientPrefab <PlayerView> (sceneSettings.playerPrefab).WhenInjectedInto <Player> ();
			Container.Bind <Player> ().ToSingle <Player> ();
			Container.Bind<int>().ToInstance(startingBullets).WhenInjectedInto<Player>();

			Container.Bind <FollowCamera>("Main").ToSinglePrefab(sceneSettings.cameraPrefab);

			Container.Bind <MapManager> ().ToSingle ();
			Container.Bind <IInitializable> ().ToSingle <MapManager> ();
			Container.Bind <Map.MapGenerator> ().ToSingle ();
			Container.Bind <Map.MeshGenerator> ().ToSinglePrefab(sceneSettings.meshGenerator).WhenInjectedInto <MapManager> ();

			Container.Bind <VirtualInput> ().ToSingle ();
			Container.Bind <IInitializable> ().ToSingle <VirtualInput> ();
			Container.Bind <DefaultInputScheme> ().ToSingle ();
			Container.Bind <IInitializable> ().ToSingle <DefaultInputScheme> ();
			Container.Bind <ITickable> ().ToSingle <DefaultInputScheme> ();

			Container.Bind <Weapon> ().ToTransient ().WhenInjectedInto <Player> ();
			Container.Bind <Bullet.Factory> ().ToSingle ();
			Container.Bind <BulletView> ().ToTransientPrefab (sceneSettings.bulletSettings.prefab).WhenInjectedInto<Bullet> ();

			Container.Bind <CanvasPresenter>().ToSinglePrefab (sceneSettings.ui);
		}

		void InstallSettings () {
			Container.Bind<Player.Settings>().ToSingleInstance(sceneSettings.playerSettings);
			Container.Bind<Map.MapGenerator.Settings>().ToSingleInstance(sceneSettings.mapSettings);
			Container.Bind<FollowCamera.Settings>().ToSingleInstance(sceneSettings.cameraSettings);
			Container.Bind<Bullet.Settings>().ToSingleInstance(sceneSettings.bulletSettings);
		}

		void InstallTriggers () {
			//Container.BindSignal <WallHitSignal>();
			//Container.BindTrigger <WallHitSignal.Trigger>().WhenInjectedInto<Bullet>();
		}


		[System.Serializable]
		public class Settings {
			public GameObject playerPrefab;
			public GameObject cameraPrefab;
			public GameObject meshGenerator;
			public GameObject ui;

			public Player.Settings playerSettings;
			public Map.MapGenerator.Settings mapSettings;
			public FollowCamera.Settings cameraSettings;
			public Bullet.Settings bulletSettings;
		}
	}

}