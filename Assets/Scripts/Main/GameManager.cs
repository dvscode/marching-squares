﻿using UnityEngine;
using System.Collections;
using Zenject;

namespace MarchingSquares {
	public class GameManager : IInitializable, IFixedTickable  {

		[Inject]
		ILogger _logger;

		Player _player;
		MapManager _world;

		public GameManager (Player player, [Inject("Main")] FollowCamera camera, MapManager world) {
			_player = player;
			_world = world;
		}

		public void Initialize () {
			_logger.Log ("GameManager > Game Started");
			_world.Generate ();
		}

		public void FixedTick () {
			_player.FixedTick ();
		}
	}
}