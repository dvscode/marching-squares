using UnityEngine;
using System.Collections;
using Map;
using Zenject;
using System;
using UniRx;

namespace MarchingSquares {
	public class MapManager : IInitializable, IDisposable
	{
		MapGenerator _generator;
		MeshGenerator _meshGenerator;

		public ReactiveProperty<int> NodesLeft {
			get; private set;
		}

		const string MapSaveKey = "MapSave";


		[Inject]
		public MapManager (MapGenerator generator, MeshGenerator meshGenerator) {
			_generator = generator;
			_meshGenerator = meshGenerator;
			_meshGenerator.CollisionEnter += HandleCollision;
			NodesLeft = new ReactiveProperty<int> (100);
		}

		public void Initialize () {
			_meshGenerator.CollisionEnter += HandleCollision;
		}

		public void Dispose () {
			_meshGenerator.CollisionEnter -= HandleCollision;
		}

		void HandleCollision (Vector3 position) {
			int x, y;
			if (_generator.FindClosestWall (position, out x, out y)) {
				RemoveWallAt (x, y);
			}
		}

		void RemoveWallAt (int x, int y) {
			if (_generator.RemoveWall (x, y)) {
				NodesLeft.Value--;
				// update mesh
				_meshGenerator.GenerateMesh (_generator.Map, 1f);
			}
		}

		public void Generate () {
			_generator.GenerateMap ();
			_meshGenerator.GenerateMesh (_generator.Map, 1f);
			NodesLeft.Value = _generator.CountWalls ();
		}

		public void SaveMap () {
			var saveData = _generator.Serialize ();
			PlayerPrefs.SetString (MapSaveKey, saveData);
			PlayerPrefs.Save ();
		}

		public void LoadMap () {
			if (PlayerPrefs.HasKey (MapSaveKey)) {
				var saveData = PlayerPrefs.GetString (MapSaveKey, "");
				if (saveData.Length > 0) {
					if (_generator.Deserialize (saveData)) {
						_meshGenerator.GenerateMesh (_generator.Map, 1f);
						NodesLeft.Value = _generator.CountWalls ();
					}
				}
			}
		}
	}
}
