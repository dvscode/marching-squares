﻿using UnityEngine;
using System.Collections;
using Zenject;

namespace MarchingSquares {
	public class FollowCamera : MonoBehaviour {

		[System.Serializable]
		public class Settings {
			public Vector3 lookOffset;
			public float followSpeed = 2f;
			public float minZoom = 20f;
			public float maxZoom = 90f;
			public float startZoom = 90f;
			public float zoomSensivity = 10f;
		}

		Settings _settings;
		Player _player;
		VirtualInput _input;

		Transform _transform;
		Transform TransformRef {
			get {
				if (_transform == null) {
					_transform = GetComponent <Transform> ();
				}
				return _transform;
			}
		}

		Camera _camera;
		public Camera CameraRef {
			get {
				if (_camera == null) {
					_camera = GetComponent <Camera> ();
				}
				return _camera;
			}
		}

		bool _isDragging;
		bool _isFollowing;
		Vector3 _dragOffset;
		Vector2 _lastTouchPoint;
		Vector3 _dragWorldPoint;
		Vector3 _dragViewportPoint;
		Vector3 _panOffset;
		float _panResetTime;

		float _zoom;

		[PostInject]
		public void Initialize (Settings settings, Player player, VirtualInput input) {
			_settings = settings;
			_player = player;
			_input = input;
			_dragOffset = Vector3.zero;
			_lastTouchPoint = Vector2.zero;
			_dragWorldPoint = Vector3.zero;
			_dragViewportPoint = Vector3.zero;
			_panOffset = Vector3.zero;
			_isDragging = false;
			_isFollowing = true;
			_panResetTime = -1f;
			_zoom = _settings.startZoom;
		}

		void FixedUpdate () {

			ValidateDragging ();

			if ((_input.GetAxis(VirtualInput.Axis.Horizontal) != 0 || _input.GetAxis (VirtualInput.Axis.Vertical) != 0)) {
				_isFollowing = true;
				_isDragging = false;
			}

			if (_isDragging) {
				UpdateDragMode ();
			} else if (_isFollowing) {
				UpdateFollowMode ();
			} else {
				// waiting for pan reset
				if (Time.realtimeSinceStartup >= _panResetTime) {
					_panOffset = Vector3.zero;
					_dragOffset = Vector3.zero;
					_isFollowing = true;
				}
			}

			UpdateZoom ();
		}

		void ValidateDragging () {
			bool drag = false;
			Vector2 touchPoint = Vector2.zero;
			
			if (Input.touchCount == 1) {
				drag = true;
				touchPoint = Input.GetTouch (0).position;
			} else if (Input.GetMouseButton (0)) {
				drag = true;
				touchPoint = Input.mousePosition;
			}

			Vector2 touchDelta = _lastTouchPoint - touchPoint;
			_lastTouchPoint = touchPoint;

			if (drag) {
				if (!_isDragging) {
					// drag start
					Vector3 worldPoint = Vector3.zero;
					TransformRef.position = _player.Position + _settings.lookOffset + _panOffset;
					if (ConvertScreenToWorldPoint (touchPoint, out worldPoint)) {
						// valid dragging point, remember starting params
						_isDragging = true;
						_dragWorldPoint = worldPoint;
						_dragViewportPoint = CameraRef.WorldToViewportPoint (_dragWorldPoint);
					}
				} else {
					// drag continues
					var touchViewportOffset = CameraRef.ScreenToViewportPoint (touchDelta);
					var worldOffset = CameraRef.ViewportToWorldPoint (_dragViewportPoint + touchViewportOffset) - _dragWorldPoint;
					_dragOffset = worldOffset;
				}
			} else {
				if (_isDragging) {
					_isDragging = false;
					_panResetTime = Time.realtimeSinceStartup + 5f;
					_panOffset += _dragOffset;
					_dragOffset = Vector3.zero;
					_isFollowing = false;
				}
			}
		}

		void UpdateDragMode () {
			Vector3 targetPosition = new Vector3 (_player.Position.x + _settings.lookOffset.x + _panOffset.x + _dragOffset.x,
			                                      _player.Position.y + _settings.lookOffset.y,
			                                      _player.Position.z + _settings.lookOffset.z + _panOffset.z + _dragOffset.z
			                                      );
			TransformRef.position = targetPosition;
		}

		void UpdateFollowMode () {
			Vector3 targetPosition = _player.Position + _settings.lookOffset;
			TransformRef.position = Vector3.Lerp (TransformRef.position, targetPosition, Time.smoothDeltaTime * _settings.followSpeed);
		}

		void UpdateZoom () {
			var zoomDelta = _input.GetAxis (VirtualInput.Axis.Zoom);
			if (zoomDelta != 0) {
				_zoom =  Mathf.Clamp(_zoom + zoomDelta * _settings.zoomSensivity, _settings.minZoom, _settings.maxZoom);
			}
			CameraRef.fieldOfView = Mathf.Lerp (CameraRef.fieldOfView, _zoom, Time.smoothDeltaTime * 10f);
		}

		
		bool ConvertScreenToWorldPoint (Vector3 screenPoint, out Vector3 worldPoint) {
			RaycastHit hit;
			if (Physics.Raycast (CameraRef.ScreenPointToRay (screenPoint), out hit)) {
				worldPoint = hit.point;
				return true;
			}
			worldPoint = Vector3.zero;
			return false;
		}
	}
}
