﻿using UnityEngine;
using System.Collections;
using UniRx;
using UnityEngine.UI;
using Zenject;
using UnityEngine.SceneManagement;

namespace MarchingSquares {
	public class CanvasPresenter : MonoBehaviour {

		public Button saveButton;
		public Button loadButton;
		public Button resetButton;
		public Text bulletLabel;
		public Text nodeLabel;

		[PostInject]
		void Initalize (Player player, MapManager world) {
			player.Bullets.SubscribeToText (bulletLabel, value => "Bullets: " + value.ToString());
			world.NodesLeft.SubscribeToText (nodeLabel, value => "Nodes: " + value.ToString ());

			saveButton.OnClickAsObservable ()
				.Subscribe (_ => {
					world.SaveMap();
				});

			loadButton.OnClickAsObservable ()
				.Subscribe (_ => {
					world.LoadMap();
				});

			resetButton.OnClickAsObservable ()
				.Subscribe (_ => {
					ZenUtil.LoadScene (SceneManager.GetActiveScene().name, delegate (DiContainer container) {						
						container.Bind<int>().ToInstance(player.Bullets.Value).WhenInjectedInto<MainInstaller>();
					});
				});
		}
	}
}