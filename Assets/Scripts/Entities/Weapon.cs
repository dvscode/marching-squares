﻿using UnityEngine;
using System.Collections;
using Zenject;
using System;
using UniRx;

namespace MarchingSquares {
	public class Weapon : IFixedTickable {

		Bullet.Factory _bulletFactory;
		Bullet _bullet;

		public ReactiveProperty<int> Bullets {
			get; private set;
		}

		public Weapon (Bullet.Factory bulletFactory) {
			_bulletFactory = bulletFactory;
			Bullets = new ReactiveProperty<int> (100);
		}

		public void Fire (Vector3 origin, Vector3 direction) {
			if (CanFire) {
				Bullets.Value --;
				_bullet = _bulletFactory.Create ();
				_bullet.LifeTimeout += OnBulletTimeout;
				_bullet.BulletHit += OnBulletHit;
				_bullet.Launch (origin, direction);
			}
		}

		public void OnBulletTimeout () {
			DestroyBullet ();
		}

		void OnBulletHit () {
			DestroyBullet ();
		}

		void DestroyBullet () {
			_bullet.LifeTimeout -= OnBulletTimeout;
			_bullet.BulletHit -= OnBulletHit;
			_bullet.Destroy ();
			_bullet = null;
		}
		
		public void FixedTick () {
			if (_bullet != null) {
				_bullet.FixedTick ();
			}
		}

		bool CanFire {
			get {
				return _bullet == null && Bullets.Value > 0;
			}
		}
	}
}
