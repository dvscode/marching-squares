﻿using UnityEngine;
using System.Collections;
using Zenject;
using UniRx;
using System;

namespace MarchingSquares {
	public class Bullet : IFixedTickable, IInitializable {

		public System.Action LifeTimeout = delegate {};
		public System.Action BulletHit = delegate {};

		[System.Serializable]
		public class Settings {
			public GameObject prefab;
			public float colliderSize = 0.2f;
			public float speed = 5f;
			public float maxLifeTime = 1f;
		}

		Settings _settings;
		BulletView _view;

		Vector3 _direction;

		[Inject]
		ILogger _logger;

		public Bullet (Settings settings, BulletView view) {
			_settings = settings;
			_view = view;
			_view.Initialize (settings.colliderSize);
		}

		[PostInject]
		public void Initialize () {
			var trigger = _view.gameObject.AddComponent <ObservableBulletHitTrigger>();
			trigger.OnBulletHitAsObservable()
				.RepeatUntilDestroy (_view.gameObject)
				.Subscribe (_ => {
					// penetrate target 
					//_view.Position = _view.Position + _direction * _settings.colliderSize;
					Debug.DrawLine (_view.Position, _view.Position + _direction * _settings.colliderSize, Color.red, 10f);
					BulletHit ();
				});
			
			Observable.Timer (TimeSpan.FromSeconds (_settings.maxLifeTime))
				.RepeatUntilDestroy (_view.gameObject)
				.Subscribe (_ => {
					LifeTimeout();
				});
		}

		public void Destroy () {
			GameObject.Destroy (_view.gameObject);
		}

		public void Launch (Vector3 position, Vector3 direction) {
			_view.Teleport (position);
			_direction = direction;
		}

		public void FixedTick () {
			_view.Move (_view.Position + _direction * _settings.speed * Time.fixedDeltaTime);
		}

		public class Factory : Factory<Bullet> {
		}
	}
}
