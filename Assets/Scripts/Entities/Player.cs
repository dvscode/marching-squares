﻿using UnityEngine;
using Zenject;
using UniRx;

namespace MarchingSquares {
	public class Player : IFixedTickable {

		[System.Serializable]
		public class Settings {
			public float movementSpeed = 1f;
			public float colliderSize = 1f;
		}
		Settings _settings;

		[Inject]
		ILogger _logger;

		PlayerView _view;
		VirtualInput _input;
		Weapon _weapon;

		public Vector3 Position {
			get {
				return _view.Position;
			}
		}

		Vector2 _lastDirection;

		public ReactiveProperty<int> Bullets {
			get {
				return _weapon.Bullets;
			}
		}

		public Player (PlayerView view, Settings settings, VirtualInput input, Weapon weapon, int bulletCount) {
			_view = view;
			_settings = settings;
			_input = input;
			_weapon = weapon;
			_view.Initialize (settings.colliderSize);
			_weapon.Bullets.Value = bulletCount;
		}

		public void ResetPosition () {
			_view.SetPosition (Vector3.zero);
		}

		public void FixedTick () {
			Vector2 movement = new Vector2 (_input.GetAxis (VirtualInput.Axis.Horizontal), 
			                                _input.GetAxis (VirtualInput.Axis.Vertical));

			if (movement.x != 0 || movement.y != 0) {
				movement.Normalize ();
				MoveRaw (movement);
			}

			if (_input.GetButton (VirtualInput.Button.Fire)) {
				_weapon.Fire (Position, new Vector3(_lastDirection.x, 0, _lastDirection.y));
			}

			_weapon.FixedTick ();
		}

		void MoveRaw (Vector2 direction) {
			var speed = direction * _settings.movementSpeed * Time.fixedDeltaTime;
			var newPosition = Position;
			newPosition.x += speed.x;
			newPosition = _view.MoveToIfPossible (newPosition);
			newPosition.z += speed.y;
			_view.MoveToIfPossible (newPosition);
			_lastDirection = direction;
		}
	}
}
