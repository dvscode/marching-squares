using Zenject.Commands;
using UnityEngine;

namespace MarchingSquares {
	public class WallHitSignal : Signal<Vector3> {
		public class Trigger : TriggerBase {}
	}
}