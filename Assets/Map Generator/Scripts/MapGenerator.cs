﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Zenject;
using System.Linq;

namespace Map {
	public class MapGenerator {

		int[,] _map;
		public int [,] Map {
			get {
				return _map;
			}
		}

		[Serializable]
		public class Settings {
			public int width;
			public int height;
			
			public string seed;
			public bool useRandomSeed;
			
			[Range(0,100)]
			public int randomFillPercent;
		}

		Settings _settings;
		public Settings MapSettings {
			get {
				return _settings;
			}
		}

		class NodeState {
			public int x {
				get; private set;
			}
			public int y {
				get; private set;
			}
			public int state {
				get; private set;
			}

			public NodeState () {
			}

			public NodeState (int x, int y, int state) {
				this.x = x;
				this.y = y;
				this.state = state;
			}

			public String Serialize () {
				return x.ToString() + ":" + y.ToString() + ":" + state.ToString();
			}

			public static NodeState Deserialize (string data) {
				var node = new NodeState ();
				var parts = data.Split (':');
				try {
					node.x = int.Parse (parts[0]);
					node.y = int.Parse (parts[1]);
					node.state = int.Parse (parts[2]);
				}
				catch (Exception e) {
					Debug.Log ("Invalid node state data: " + data + ", err: " + e.Message);
				}
				return node;
			}
		}
		List<NodeState> changedNodes;

		public MapGenerator (Settings settings) {
			_settings = settings;
			changedNodes = new List<NodeState> ();
		}

		public void GenerateMap() {
			_map = new int[_settings.width, _settings.height];
			RandomFillMap();

			for (int i = 0; i < 5; i ++) {
				SmoothMap();
			}

			ProcessMap ();

			// ensure that 5x5 area in the center of the map is empty (for the player)
			for (int x = -2; x <= 2; x++) {
				for (int y = -2; y <= 2; y++) {
					_map[x + _settings.width / 2, y + _settings.height / 2] = 0;
				}
			}

			int borderSize = 1;
			int[,] borderedMap = new int[_settings.width + borderSize * 2, _settings.height + borderSize * 2];

			for (int x = 0; x < borderedMap.GetLength(0); x ++) {
				for (int y = 0; y < borderedMap.GetLength(1); y ++) {
					if (x >= borderSize && x < _settings.width + borderSize && y >= borderSize && y < _settings.height + borderSize) {
						borderedMap[x,y] = _map[x-borderSize,y-borderSize];
					}
					else {
						borderedMap[x,y] =1;
					}
				}
			}

			// apply changed tiles
			for (int nodeIdx = 0; nodeIdx < changedNodes.Count; nodeIdx++) {
				var node = changedNodes[nodeIdx];
				borderedMap[node.x, node.y] = node.state;
			}

			_map = borderedMap;
		}

		void ProcessMap() {
			List<List<Coord>> wallRegions = GetRegions (1);
			int wallThresholdSize = 50;

			foreach (List<Coord> wallRegion in wallRegions) {
				if (wallRegion.Count < wallThresholdSize) {
					foreach (Coord tile in wallRegion) {
						_map[tile.tileX,tile.tileY] = 0;
					}
				}
			}

			List<List<Coord>> roomRegions = GetRegions (0);
			int roomThresholdSize = 50;
			List<Room> survivingRooms = new List<Room> ();
			
			foreach (List<Coord> roomRegion in roomRegions) {
				if (roomRegion.Count < roomThresholdSize) {
					foreach (Coord tile in roomRegion) {
						_map[tile.tileX,tile.tileY] = 1;
					}
				}
				else {
					survivingRooms.Add(new Room(roomRegion, _map));
				}
			}
			survivingRooms.Sort ();
			survivingRooms [0].isMainRoom = true;
			survivingRooms [0].isAccessibleFromMainRoom = true;

			ConnectClosestRooms (survivingRooms);
		}

		void ConnectClosestRooms(List<Room> allRooms, bool forceAccessibilityFromMainRoom = false) {

			List<Room> roomListA = new List<Room> ();
			List<Room> roomListB = new List<Room> ();

			if (forceAccessibilityFromMainRoom) {
				foreach (Room room in allRooms) {
					if (room.isAccessibleFromMainRoom) {
						roomListB.Add (room);
					} else {
						roomListA.Add (room);
					}
				}
			} else {
				roomListA = allRooms;
				roomListB = allRooms;
			}

			int bestDistance = 0;
			Coord bestTileA = new Coord ();
			Coord bestTileB = new Coord ();
			Room bestRoomA = new Room ();
			Room bestRoomB = new Room ();
			bool possibleConnectionFound = false;

			foreach (Room roomA in roomListA) {
				if (!forceAccessibilityFromMainRoom) {
					possibleConnectionFound = false;
					if (roomA.connectedRooms.Count > 0) {
						continue;
					}
				}

				foreach (Room roomB in roomListB) {
					if (roomA == roomB || roomA.IsConnected(roomB)) {
						continue;
					}
				
					for (int tileIndexA = 0; tileIndexA < roomA.edgeTiles.Count; tileIndexA ++) {
						for (int tileIndexB = 0; tileIndexB < roomB.edgeTiles.Count; tileIndexB ++) {
							Coord tileA = roomA.edgeTiles[tileIndexA];
							Coord tileB = roomB.edgeTiles[tileIndexB];
							int distanceBetweenRooms = (int)(Mathf.Pow (tileA.tileX-tileB.tileX,2) + Mathf.Pow (tileA.tileY-tileB.tileY,2));

							if (distanceBetweenRooms < bestDistance || !possibleConnectionFound) {
								bestDistance = distanceBetweenRooms;
								possibleConnectionFound = true;
								bestTileA = tileA;
								bestTileB = tileB;
								bestRoomA = roomA;
								bestRoomB = roomB;
							}
						}
					}
				}
				if (possibleConnectionFound && !forceAccessibilityFromMainRoom) {
					CreatePassage(bestRoomA, bestRoomB, bestTileA, bestTileB);
				}
			}

			if (possibleConnectionFound && forceAccessibilityFromMainRoom) {
				CreatePassage(bestRoomA, bestRoomB, bestTileA, bestTileB);
				ConnectClosestRooms(allRooms, true);
			}

			if (!forceAccessibilityFromMainRoom) {
				ConnectClosestRooms(allRooms, true);
			}
		}

		void CreatePassage(Room roomA, Room roomB, Coord tileA, Coord tileB) {
			Room.ConnectRooms (roomA, roomB);
			//Debug.DrawLine (CoordToWorldPoint (tileA), CoordToWorldPoint (tileB), Color.green, 100);

			List<Coord> line = GetLine (tileA, tileB);
			foreach (Coord c in line) {
				DrawCircle(c,5);
			}
		}

		void DrawCircle(Coord c, int r) {
			for (int x = -r; x <= r; x++) {
				for (int y = -r; y <= r; y++) {
					if (x*x + y*y <= r*r) {
						int drawX = c.tileX + x;
						int drawY = c.tileY + y;
						if (IsInMapRange(drawX, drawY)) {
							_map[drawX,drawY] = 0;
						}
					}
				}
			}
		}

		List<Coord> GetLine(Coord from, Coord to) {
			List<Coord> line = new List<Coord> ();

			int x = from.tileX;
			int y = from.tileY;

			int dx = to.tileX - from.tileX;
			int dy = to.tileY - from.tileY;

			bool inverted = false;
			int step = Math.Sign (dx);
			int gradientStep = Math.Sign (dy);

			int longest = Mathf.Abs (dx);
			int shortest = Mathf.Abs (dy);

			if (longest < shortest) {
				inverted = true;
				longest = Mathf.Abs(dy);
				shortest = Mathf.Abs(dx);

				step = Math.Sign (dy);
				gradientStep = Math.Sign (dx);
			}

			int gradientAccumulation = longest / 2;
			for (int i =0; i < longest; i ++) {
				line.Add(new Coord(x,y));

				if (inverted) {
					y += step;
				}
				else {
					x += step;
				}

				gradientAccumulation += shortest;
				if (gradientAccumulation >= longest) {
					if (inverted) {
						x += gradientStep;
					}
					else {
						y += gradientStep;
					}
					gradientAccumulation -= longest;
				}
			}

			return line;
		}

		Vector3 CoordToWorldPoint(Coord tile) {
			return new Vector3 (-_settings.width / 2 + .5f + tile.tileX, 2, -_settings.height / 2 + .5f + tile.tileY);
		}

		List<List<Coord>> GetRegions(int tileType) {
			List<List<Coord>> regions = new List<List<Coord>> ();
			int[,] mapFlags = new int[_settings.width, _settings.height];

			for (int x = 0; x < _settings.width; x ++) {
				for (int y = 0; y < _settings.height; y ++) {
					if (mapFlags[x,y] == 0 && _map[x,y] == tileType) {
						List<Coord> newRegion = GetRegionTiles(x,y);
						regions.Add(newRegion);

						foreach (Coord tile in newRegion) {
							mapFlags[tile.tileX, tile.tileY] = 1;
						}
					}
				}
			}

			return regions;
		}

		List<Coord> GetRegionTiles(int startX, int startY) {
			List<Coord> tiles = new List<Coord> ();
			int[,] mapFlags = new int[_settings.width, _settings.height];
			int tileType = _map [startX, startY];

			Queue<Coord> queue = new Queue<Coord> ();
			queue.Enqueue (new Coord (startX, startY));
			mapFlags [startX, startY] = 1;

			while (queue.Count > 0) {
				Coord tile = queue.Dequeue();
				tiles.Add(tile);

				for (int x = tile.tileX - 1; x <= tile.tileX + 1; x++) {
					for (int y = tile.tileY - 1; y <= tile.tileY + 1; y++) {
						if (IsInMapRange(x,y) && (y == tile.tileY || x == tile.tileX)) {
							if (mapFlags[x,y] == 0 && _map[x,y] == tileType) {
								mapFlags[x,y] = 1;
								queue.Enqueue(new Coord(x,y));
							}
						}
					}
				}
			}
			return tiles;
		}

		bool IsInMapRange(int x, int y) {
			return x >= 0 && x < _settings.width && y >= 0 && y < _settings.height;
		}


		void RandomFillMap() {
			if (_settings.useRandomSeed) {
				_settings.seed = Time.time.ToString();
			}

			System.Random pseudoRandom = new System.Random(_settings.seed.GetHashCode());

			for (int x = 0; x < _settings.width; x ++) {
				for (int y = 0; y < _settings.height; y ++) {
					if (x == 0 || x == _settings.width-1 || y == 0 || y == _settings.height -1) {
						_map[x,y] = 1;
					}
					else {
						_map[x,y] = (pseudoRandom.Next(0,100) < _settings.randomFillPercent)? 1: 0;
					}
				}
			}
		}

		void SmoothMap() {
			for (int x = 0; x < _settings.width; x ++) {
				for (int y = 0; y < _settings.height; y ++) {
					int neighbourWallTiles = GetSurroundingWallCount(x,y);

					if (neighbourWallTiles > 4)
						_map[x,y] = 1;
					else if (neighbourWallTiles < 4)
						_map[x,y] = 0;

				}
			}
		}

		/**
		 * Removes wall at x/y, returns true if map changed
		 */
		public bool RemoveWall (int x, int y) {
			if (IsInMapRange (x, y)) {
				if (_map [x, y] != 0 && x > 0 && x <= _settings.width - 1 && y > 0 && y <= _settings.height - 1) {
					_map [x, y] = 0;
					changedNodes.Add (new NodeState (x, y, 0));
					return true;
				}
			}
			return false;
		}

		public string Serialize () {
			// serialize to string
			var data = new System.Text.StringBuilder ();
			// line 0 - seed
			// line 1 - nodes
			// line 2 - hash code
			data.AppendLine (_settings.seed.ToString ());
			data.AppendLine (string.Join ("|", changedNodes.ConvertAll<string>(node => { return node.Serialize(); }).ToArray()));
			data.AppendLine (Convert.ToString (data.ToString().GetHashCode(), 16));
			return data.ToString ();
		}

		public bool Deserialize (string data) {
			var reader = new System.IO.StringReader (data);
			// validate data
			if (data.Length == 0) return false;

			var seed = reader.ReadLine ();
			var nodeData = reader.ReadLine ();
			var hash = reader.ReadLine ();

			if (hash != Convert.ToString ((seed + "\n" + nodeData + "\n").GetHashCode(), 16)) {
				return false;
			}

			// parse
			_settings.seed = seed;
			var nodes = nodeData.Split ('|');
			if (nodes.Length > 0) {
				var nodeList = nodes.ToList<string>();
				changedNodes = nodeList.ConvertAll <NodeState> (x => { return NodeState.Deserialize (x); });
			} else {
				changedNodes = new List<NodeState>();
			}

			_settings.useRandomSeed = false;
			GenerateMap ();

			return true;
		}

		public bool FindClosestWall (Vector3 position, out int x, out int y) {
			float minDistance = 1.5f;
			int bestX = -1, bestY = -1;
			bool wallFound = false;
			for (float offsetX = -1f; offsetX <= 1f; offsetX += 0.5f) {
				for (float offsetY = -1f; offsetY <= 1f; offsetY += 0.5f) {
					int tileX = Mathf.RoundToInt (position.x + _settings.width / 2 + offsetX);
					int tileY = Mathf.RoundToInt (position.z + _settings.height / 2 + offsetY);
					float distanceSq = Mathf.Pow(tileX - _settings.width / 2 - position.x, 2) + Mathf.Pow(tileY - _settings.height / 2 - position.z, 2);
					if (IsInMapRange (tileX, tileY) && _map [tileX, tileY] != 0 && distanceSq <= minDistance) {
						bestX = tileX;
						bestY = tileY;
						minDistance = distanceSq;
						wallFound = true;
					}
				}
			}
			x = bestX;
			y = bestY;
			return wallFound;
		}

		int GetSurroundingWallCount(int gridX, int gridY) {
			int wallCount = 0;
			for (int neighbourX = gridX - 1; neighbourX <= gridX + 1; neighbourX ++) {
				for (int neighbourY = gridY - 1; neighbourY <= gridY + 1; neighbourY ++) {
					if (IsInMapRange(neighbourX,neighbourY)) {
						if (neighbourX != gridX || neighbourY != gridY) {
							wallCount += _map[neighbourX,neighbourY];
						}
					}
					else {
						wallCount ++;
					}
				}
			}

			return wallCount;
		}

		public int CountWalls () {
			// count all walls, except border
			int wallCount = 0;
			for (int x = 1; x < _settings.width - 1; x ++) {
				for (int y = 1; y < _settings.height - 1; y ++) {
					if (_map[x, y] == 1) {
						wallCount++;
					}
				}
			}
			return wallCount;
		}

		struct Coord {
			public int tileX;
			public int tileY;

			public Coord(int x, int y) {
				tileX = x;
				tileY = y;
			}
		}


		class Room : IComparable<Room> {
			public List<Coord> tiles;
			public List<Coord> edgeTiles;
			public List<Room> connectedRooms;
			public int roomSize;
			public bool isAccessibleFromMainRoom;
			public bool isMainRoom;

			public Room() {
			}

			public Room(List<Coord> roomTiles, int[,] map) {
				tiles = roomTiles;
				roomSize = tiles.Count;
				connectedRooms = new List<Room>();

				edgeTiles = new List<Coord>();
				foreach (Coord tile in tiles) {
					for (int x = tile.tileX-1; x <= tile.tileX+1; x++) {
						for (int y = tile.tileY-1; y <= tile.tileY+1; y++) {
							if (x == tile.tileX || y == tile.tileY) {
								if (map[x,y] == 1) {
									edgeTiles.Add(tile);
								}
							}
						}
					}
				}
			}

			public void SetAccessibleFromMainRoom() {
				if (!isAccessibleFromMainRoom) {
					isAccessibleFromMainRoom = true;
					foreach (Room connectedRoom in connectedRooms) {
						connectedRoom.SetAccessibleFromMainRoom();
					}
				}
			}

			public static void ConnectRooms(Room roomA, Room roomB) {
				if (roomA.isAccessibleFromMainRoom) {
					roomB.SetAccessibleFromMainRoom ();
				} else if (roomB.isAccessibleFromMainRoom) {
					roomA.SetAccessibleFromMainRoom();
				}
				roomA.connectedRooms.Add (roomB);
				roomB.connectedRooms.Add (roomA);
			}

			public bool IsConnected(Room otherRoom) {
				return connectedRooms.Contains(otherRoom);
			}

			public int CompareTo(Room otherRoom) {
				return otherRoom.roomSize.CompareTo (roomSize);
			}


		}
	}
}
